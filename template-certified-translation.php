<?php
/**
 * Template Name: Certified Translation Landing 2
 */
get_header(); ?>

<div class="clear"></div>

</header> <!-- / END HOME SECTION  -->

<div id="content" class="site-content">

	<div class="container">

		<!-- <div class="content-left-wrap col-md-12">

			<div id="primary" class="content-area">

				<main id="main" class="site-main" role="main"> -->

					<?php 
					while ( have_posts() ) : the_post(); 

					get_template_part( 'content', 'page-no-title' );

							// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || '0' != get_comments_number() ) :
						comments_template();
					endif;

					endwhile;
					?>

				<!-- </main>

			</div>

		</div> -->

		

	</div><!-- .container -->

	<section class="about-us" id="aboutus">
		<div class="container">

			<!-- SECTION HEADER -->

			<div class="section-header">

				<!-- SECTION TITLE -->

				
				<!-- SHORT DESCRIPTION ABOUT THE SECTION -->

			</div><!-- / END SECTION HEADER -->

			<!-- 3 COLUMNS OF ABOUT US-->

			<div class="row">

				<!-- COLUMN 1 - BIG MESSAGE ABOUT THE COMPANY-->

				<div class="col-lg-12 col-md-12 column zerif_about_us_center " data-scrollreveal="enter bottom after 0s over 1s" data-sr-init="true" data-sr-complete="true"><p></p><div class="row">
					<div class="col-md-6">
						<h2>Ready to get started?</h2>
					</div>
					<div class="col-md-6">
						<a class="start-order" href="/order-translation/"><img src="/wp-content/uploads/2016/06/start-order-green.png"></a>
					</div>
				</div><p></p></div>
			</div> <!-- / END 3 COLUMNS OF ABOUT US-->

			<!-- CLIENTS -->
			
		</div> <!-- / END CONTAINER -->

	</section>

<?php get_footer(); ?>