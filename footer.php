<?php
/**
 * The template for displaying the footer.
 * Contains the closing of the #content div and all content after
 */

$footer_background_color = '';
if (  is_page( 161 ) ) {
	$footer_background_color = '#e8eff7';
} elseif ( is_page( 14 ) ) {
	$footer_background_color = '#ffffff';
} else {
	$footer_background_color = '#d1d1d1';
}

?>

</div><!-- .site-content -->

<footer id="footer"  role="contentinfo" style="background: <?php echo $footer_background_color; ?>;">


	<div class="container">

		<?php if ( is_page( 14 ) ): ?>

			<img class="footer_logo" src="<?php echo get_stylesheet_directory_uri(); ?>/images/footer_logo.png" alt="footer_logo.png">
			<div class="footer_copyright">ASAP Translate - Certified Translations</div>
			<!-- <div class="footer-address">2425 N University Dr 2423 Coral Springs FL 33065</div> -->

		<?php endif; ?>

		

		<?php wp_nav_menu( array( 
			'theme_location' => 'footer-menu',
			'after' => '<span class="pipe"> | </span>' 
			) 
		); 
		?>

		<?php if ( !is_page( 14 ) ): ?>

			<div class="row">
				<p>© 2016 ASAP Translate. All Rights Reserved</p>
				<div class="col-md-2 col-md-offset-1">
					<img src="/wp-content/uploads/2016/06/uscis-logo-grayscale.png">
				</div>
				<div class="col-md-2">
					<img src="/wp-content/uploads/2016/06/ata-logo-footer-grayscale.png">
				</div>
				<div class="col-md-2">
					<!-- DO NOT EDIT - GlobalSign SSL Site Seal Code - DO NOT EDIT -->
					<table width="125" border="0" cellspacing="0" cellpadding="0" title="CLICK TO VERIFY: This site uses a GlobalSign SSL Certificate to secure your personal information."><tbody><tr><td><script src="//ssif1.globalsign.com/SiteSeal/siteSeal/siteSeal/siteSeal.do?p1=www.asaptranslate.com&amp;p2=SZ125-50&amp;p3=image&amp;p4=en&amp;p5=V0021&amp;p6=S001&amp;p7=https"></script><span> <a id="aa" href="javascript:ss_open_sub()"><img name="ss_imgTag" border="0" src="//seal.alphassl.com/SiteSeal/siteSeal/siteSeal/siteSealImage.do?p1=www.asaptranslate.com&amp;p2=SZ125-50&amp;p3=image&amp;p4=en&amp;p5=V0021&amp;p6=S001&amp;p7=https&amp;deterDn=" alt="Please click to see profile." oncontextmenu="return false;" galleryimg="no"></a></span><span id="ss_siteSeal_fin_SZ125-50_image_en_V0021_S001"></span><script type="text/javascript" src="//seal.globalsign.com/SiteSeal/gmogs_image_125-50_en_black.js"></script></td></tr></tbody></table>
					<!-- DO NOT EDIT - GlobalSign SSL Site Seal Code - DO NOT EDIT -->
				</div>
				<div class="col-md-2">
					<img src="/wp-content/uploads/2016/06/shopper-approved-logo.png">
				</div>
				<div class="col-md-2">
					<img src="/wp-content/uploads/2016/06/paypal-secure-logo.png">
				</div>
			</div>

		<?php endif; ?>


	</div> <!-- / END CONTAINER -->

</footer> <!-- / END FOOOTER  -->


</div><!-- mobile-bg-fix-whole-site -->
</div><!-- .mobile-bg-fix-wrap -->


<?php wp_footer(); ?>

</body>

</html>