<?php

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles',99);
function child_enqueue_styles() {
    $parent_style = 'parent-style';
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',get_stylesheet_directory_uri() . '/custom.css', array( $parent_style ));
    wp_dequeue_style( 'zerif_font' );
    wp_enqueue_style( 'asaptranslate_font', asaptranslate_slug_fonts_url(), array(), null );
}

function asaptranslate_slug_fonts_url() {
    $fonts_url = '';
     /* Translators: If there are characters in your language that are not
    * supported by Lora, translate this to 'off'. Do not translate
    * into your own language.
    */
     $lato = _x( 'on', 'Lato font: on or off', 'zerif-lite' );
     $homemade = _x( 'on', 'Homemade font: on or off', 'zerif-lite' );
    /* Translators: If there are characters in your language that are not
    * supported by Open Sans, translate this to 'off'. Do not translate
    * into your own language.
    */
    $monserrat = _x( 'on', 'Monserrat font: on or off', 'zerif-lite' );

    $zerif_use_safe_font = get_theme_mod('zerif_use_safe_font');
    
    if ( ( 'off' !== $lato || 'off' !== $monserrat || 'off' !== $homemade ) && isset($zerif_use_safe_font) && ($zerif_use_safe_font != 1) ) {
        $font_families = array();

        if ( 'off' !== $lato ) {
            $font_families[] = 'Lato:300,400,700,400italic';
        }
        if ( 'off' !== $monserrat ) {
            $font_families[] = 'Montserrat:700,400';
        }
        
        if ( 'off' !== $homemade ) {
            $font_families[] = 'Homemade Apple';
        }
        $query_args = array(
            'family' => urlencode( implode( '|', $font_families ) ),
            'subset' => urlencode( 'latin,latin-ext' ),
            );
        $fonts_url = add_query_arg( $query_args, '//fonts.googleapis.com/css' );
    }
    return $fonts_url;
}


if ( get_stylesheet() !== get_template() ) {
    add_filter( 'pre_update_option_theme_mods_' . get_stylesheet(), function ( $value, $old_value ) {
       update_option( 'theme_mods_' . get_template(), $value );
         return $old_value; // prevent update to child theme mods
     }, 10, 2 );
    add_filter( 'pre_option_theme_mods_' . get_stylesheet(), function ( $default ) {
        return get_option( 'theme_mods_' . get_template(), $default );
    } );
}


/* 
* Add or Remove on init.
*/
add_action( 'init', 'p_asapt_add_remove_init' );
function p_asapt_add_remove_init() {

    // Register footer menu.
    register_nav_menu( 'footer-menu', __( 'Footer Menu' ) );

    // Remove Zopim widget from other pages than the homepage.
    if ( $_SERVER["REQUEST_URI"] != '/' ) {
        remove_action( 'wp_footer', array( 'Zopim_Widget', 'zopimme' ) );
    }

}








?>